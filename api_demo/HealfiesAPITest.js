// instalation configuration
var thirdPartyConfig = {
  healfiesApi: 'https://api.healfies.com',
  token: 'yourtolken',
  authKey: 'yourkey'
}

// derived from configuration
var uploadFromUrlUrl = thirdPartyConfig.healfiesApi + '/public/v1/upload/fromUrl'
var uploadUrl = thirdPartyConfig.healfiesApi + '/public/v1/upload'
var resultsUrl = thirdPartyConfig.healfiesApi + '/public/v1/results'
var deliveryUrlPt1 = thirdPartyConfig.healfiesApi + '/public/v1/results/'
var deliveryUrlPt2 = '/send'

var token = 'Basic ' + btoa(thirdPartyConfig.token + ':' + thirdPartyConfig.authKey)

// user input
var files = null
var fileIds = null

// misc
var ajax

// UI related functions
var setFiles = function (element) {
  console.log('files:', element.files)
  // Turn the FileList object into an Array
  files = []
  for (var i = 0; i < element.files.length; i++) {
    files.push(element.files[i])
  }
}

var submitResult = function () {
  console.log('Submiting result...')
  files = files ? files : []
  fileIds = []
  uploadFile(0, createResult)
}

// Server related functions
var uploadFile = function (index, cb) {
  if (index < files.length) {
    var uploadForm = new FormData()
    uploadForm.append('qqfile', files[index])

    buildAjaxRequest('POST', uploadUrl, function () {
      if (ajax.readyState != 4) return
      if (ajax.status == 200) {
        var response = JSON.parse(ajax.responseText)
        $('#outputText').append('&#xA; Uploaded file with healfies id=' + response.details.id + '&#xA;')
        fileIds.push(response.details.id)
      } else {
        $('#outputText').append('&#xA; Error uploading file: ' + JSON.stringify(ajax.responseText) + '&#xA;')
        console.log('error: ' + ajax.status, ajax.responseText)
      }
      uploadFile(index + 1, cb)
    })
    ajax.send(uploadForm)
  } else {
    downloadFile(cb)
  }
}

var downloadFile = function (cb) {
  var fileUrl = document.getElementById('fileDownloadUrl').value.trim()
  if (fileUrl.length > 0) {
    var fileName = document.getElementById('downloadFileName').value.trim()
    if (fileName == '') {
      fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1)
    }
    if (fileName == '') { // url ends with '/'
      fileName = 'sem nome'
    }

    var uploadForm = new FormData()
    uploadForm.append('fileurl', fileUrl)
    uploadForm.append('filename', fileName)

    buildAjaxRequest('POST', uploadFromUrlUrl, function () {
      if (ajax.readyState != 4) return
      if (ajax.status == 200) {
        var response = JSON.parse(ajax.responseText)
        $('#outputText').append('&#xA; Downloaded file with healfies id=' + response.details.id + '&#xA;')
        fileIds.push(response.details.id)
      } else {
        $('#outputText').append('&#xA; Error uploading file: ' + JSON.stringify(ajax.responseText) + '&#xA;')
        console.log('error: ' + ajax.status, ajax.responseText)
      }

      cb()
    })
    ajax.send(uploadForm)
  } else {
    cb()
  }
}

var createResult = function () {
  var resultsForm = {

    // Recipient Data
    'recipientName': document.getElementById('ownerInputName').value,
    'birth_date': new Date(document.getElementById('ownerInputBirth').value),
    'gender': $('input[name="gender"]:checked').val(),
    'motherName': document.getElementById('ownerInputMother').value,
    'recipientEmail': document.getElementById('ownerInputEmail').value,
    'phoneNumber': document.getElementById('ownerInputPhone').value,
    'cpf': document.getElementById('ownerInputCPF').value,
    'registroGeral': document.getElementById('ownerInputRG').value,
    'locality': document.getElementById('ownerInputLocality').value,
    'region': document.getElementById('ownerInputRegion').value,

    // Delivery Data
    'title': document.getElementById('deliveryTitle').value,
    'content': document.getElementById('messageInput').value,
    'event_date': new Date(document.getElementById('ownerInputEventDate').value),
    'accessionNumber': document.getElementById('deliveryAccessionNumber').value,
    'protocol': document.getElementById('deliveryProtocol').value,
    'password': document.getElementById('deliveryPassword').value,
    'files': fileIds,
    'rawData': document.getElementById('rawDataInput').value,

    // Delivery Sharing
    'sharingName': document.getElementById('sharingName').value,
    'sharingSpecialty': document.getElementById('sharingSpecialty').value,
    'sharingDocumentType': document.getElementById('sharingDocumentType').value,
    'sharingDocumentNumber': document.getElementById('sharingDocumentNumber').value,
    'sharingEmail': document.getElementById('sharingEmail').value,
    'sharingPhone': document.getElementById('sharingPhone').value,

    // You can send other documents with this format:
    // 'documents' : [
    //  {
    //    'type': 'RG',
    //    'number': '8746382746'
    //  }
    // ],

  // You can also send separated recipient first_name & last_name this way:
  // 'owner' : {
  //  'first_name' : 'John',
  //  'last_name' : 'Doe',
  // },
  }

  console.log('resultsForm: ', resultsForm)

  buildAjaxRequest('POST', resultsUrl, function () {
    if (ajax.readyState != 4) return
    if (ajax.status == 200) {
      console.log('result res=', ajax.responseText)
      var response = JSON.parse(ajax.responseText)
      $('#outputText').append('&#xA; Created result with healfies id=' + response.id + ' &#xA;')

      var deliverResult = $('#checkbox-1').is(':checked')
      if (deliverResult) {
        doDeliverResult(response.id)
      }
    } else {
      $('#outputText').append('&#xA; Error creating result: ' + JSON.stringify(ajax.responseText) + '&#xA;')
      console.log('error: ' + ajax.status, ajax.responseText)
    }
  })

  var body = JSON.stringify(resultsForm)
  ajax.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
  ajax.send(body)
}

var doDeliverResult = function (resultId) {
  var deliverCurrentResultUrl = deliveryUrlPt1 + resultId + deliveryUrlPt2

  buildAjaxRequest('GET', deliverCurrentResultUrl, function () {
    if (ajax.readyState != 4) return
    if (ajax.status == 200) {
      console.log('result res=', ajax.responseText)
      var response = JSON.parse(ajax.responseText)
      $('#outputText').append('&#xA; Delivered result with healfies id=' + response.id + '&#xA;')
    } else {
      $('#outputText').append('&#xA; Error delivering result: ' + JSON.stringify(ajax.responseText) + '&#xA;')
      console.log('error: ' + ajax.status, ajax.responseText)
    }
  })

  ajax.send()
}

var buildAjaxRequest = function (method, url, cb) {
  ajax = new XMLHttpRequest()
  ajax.open(method, url, true)
  ajax.setRequestHeader('authorization', token)
  ajax.onreadystatechange = cb
  return ajax
}
